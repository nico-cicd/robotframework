FROM ubuntu:20.04

COPY ./requirements.txt ./

# Desactivation du reglage creneaux horaire
ARG DEBIAN_FRONTEND=noninteractive 

# Mise a jour ubuntu
RUN apt-get -y update
RUN apt-get -y upgrade

# Install paquet facultatif
RUN apt-get install -y git nano jq curl wget

# Install dependencies library Browser
RUN apt-get install -y npm

# Suite d'installation library Browser
RUN npm install -g n
RUN n stable
RUN hash -r

# Installation de python
RUN apt install -y python3-pip

# Install Robotframework et librarie
RUN pip install --no-cache-dir -r requirements.txt

# Initialisation de la librarie browser
RUN rfbrowser init
RUN npx playwright install-deps

# Install google-chrome
RUN apt-get install -y wget
RUN wget -q https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN apt-get install -y ./google-chrome-stable_current_amd64.deb
RUN rm -f google-chrome-stable_current_amd64.deb
